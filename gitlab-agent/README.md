# Gitlab agent

https://docs.gitlab.com/ee/user/clusters/agent/install/index.html

Agenti pro K8s jsou konfigurováni na úrovni celého účtu OICT aby bylo možné je používat ve všech pod projektech.
viz https://gitlab.com/operator-ict/infrastructure/-/clusters

## Upgrade

https://docs.gitlab.com/ee/user/clusters/agent/install/index.html#update-the-agent-version

https://gitlab.com/gitlab-org/charts/gitlab-agent/-/blob/main/values.yaml

https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/container_registry/1223205?orderBy=NAME&sort=asc&search%5B%5D=15.6&search%5B%5D=

### Tag search
https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/container_registry/1223205?orderBy=NAME&sort=asc&search%5B%5D=v16.10&search%5B%5D=

# rabin
```bash
helm upgrade dp-rabin gitlab/gitlab-agent \
  --namespace gitlab-agent \
  --reuse-values \
  --values ./gitlab-agent/common.yaml \
  --set config.token=glagent-Z2za4wyZ5kfMTwjzfcq9zqf-JAToZLXwPpYqMyxhSBfiYzzriA \
  --version 2.6.2
#  --set image.tag=v17.4.0 \

```

# golem
```bash
helm upgrade dp-golem gitlab/gitlab-agent \
  --namespace gitlab-agent \
  --reuse-values \
  --values ./gitlab-agent/common.yaml \
  --version 2.6.2 
#  --set image.tag=v17.4.0 \
```