# Rabin
## Kratos
helm upgrade --install --namespace jis --values ../rabin/kratos/values-secret.yaml ory-secret ory-secret
## Keto
helm upgrade --install --namespace jis --values ../rabin/keto/values-secret.yaml ory-secret ory-secret

# Golem
## Kratos
helm upgrade --install --namespace jis --values ../golem/kratos/values-secret.yaml ory-secret ory-secret
## Keto
helm upgrade --install --namespace jis --values ../golem/keto/values-secret.yaml ory-secret ory-secret
