## Ory Oathkeeper

helm repo add ory https://k8s.ory.sh/helm/charts
helm repo update

Dev
```
helm upgrade --install -n jis --values ./cluster_development/jis_oathkeeper-values.yaml vymi-ory-oathkeeper ory/oathkeeper
```

Prod
```
helm upgrade --install -n jis --values ./cluster_production/jis_oathkeeper-values.yaml vymi-ory-oathkeeper ory/oathkeeper
```
