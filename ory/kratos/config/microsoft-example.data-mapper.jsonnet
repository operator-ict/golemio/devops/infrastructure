local claims = std.extVar('claims');

local tidToOrgMap = {
  '??': 'oict', // Available in secrets, see /security/ory/values-secret.yaml -> kratos.oidcConfigProviders (mapper_url)
  '???': 'ropid', // Available in secrets, see /security/ory/values-secret.yaml -> kratos.oidcConfigProviders (mapper_url)
  '????': 'dpp', // Available in secrets, see /security/ory/values-secret.yaml -> kratos.oidcConfigProviders (mapper_url)
  '9188040d-6c67-4c5b-b112-36a304b66dad': 'test', // Microsoft personal account tenant
  default: 'xxx'  // Default value if tid doesn't match
};

local getOrgSchemaId(tid) =
  if tid in tidToOrgMap then
    tidToOrgMap[tid]
  else
    tidToOrgMap.default;

local getUserName(claims) = 
  if 'name' in claims then
    claims.name
  else if 'preferred_username' in claims then
    claims.preferred_username
  else
    claims.email;

if 'email' in claims then 
  {
    identity: {
      traits: {
        "email": claims.email,
        "username": getUserName(claims),
      },
      metadata_public: {
        "organization_schema_id": 
          if 'raw_claims' in claims && 'tid' in claims.raw_claims then
            getOrgSchemaId(claims.raw_claims.tid)
          else
            tidToOrgMap.default
      }
    }
  }
else
  error 'Unknown metadata'
