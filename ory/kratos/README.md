## Ory Kratos

https://github.com/ory/k8s/tree/master/helm/charts/kratos

```bash
helm repo add ory https://k8s.ory.sh/helm/charts
helm repo update
```

### Dev
```bash
helm upgrade --install -n jis --values ./cluster_development/jis_kratos-values.yaml vymi-ory-kratos ory/kratos
```

### Prod
```bash
helm upgrade --install -n jis --values ./cluster_production/jis_kratos-values.yaml vymi-ory-kratos ory/kratos
```
