## Ory Keto
https://github.com/ory/k8s/tree/master/helm/charts/keto

```bash
helm repo add ory https://k8s.ory.sh/helm/charts
helm repo update
```

### Dev
```bash
helm upgrade --install -n jis --values ./cluster_development/jis_keto-values.yaml vymi-ory-keto ory/keto
```

### Prod
```bash
helm upgrade --install -n jis --values ./cluster_production/jis_keto-values.yaml vymi-ory-keto ory/keto
```
