
# Rabin
```bash
helm upgrade --install -n data-praha --values ./data-praha/values-development-versions.yaml --values ./cluster_development/data-praha_data-praha-values.yaml data-praha ./data-praha
```

# Golem
```bash
helm upgrade --install -n data-praha --values ./data-praha/values-main-versions.yaml --values ./cluster_production/data-praha_data-praha-values.yaml data-praha ./data-praha
```
