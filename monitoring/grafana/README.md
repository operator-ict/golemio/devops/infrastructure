# Grafana

https://github.com/grafana/helm-charts/tree/main/charts/grafana

```bash
helm repo add grafana https://grafana.github.io/helm-charts
helm repo update

# Rabin
helm upgrade --install -n monitoring --values cluster_development/monitoring_grafana.yaml grafana grafana/grafana --version 8.4.1

# Golem
helm upgrade --install -n monitoring --values cluster_production/monitoring_grafana.yaml grafana grafana/grafana --version 8.4.1
```


# Message templates
https://grafana.com/docs/grafana/latest/alerting/manage-notifications/template-notifications/reference/
https://grafana.com/docs/grafana/latest/alerting/manage-notifications/template-notifications/using-go-templating-language/
https://github.com/grafana/alerting/blob/main/templates/default_template.go
