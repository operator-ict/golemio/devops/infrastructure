# Azure prometheus

https://learn.microsoft.com/en-us/azure/azure-monitor/essentials/prometheus-metrics-scrape-configuration#enabling-and-disabling-default-targets

```bash
# Rabin
kubectl apply -f monitoring/prometheus/Azure\ prometheus/ama-metrics-prometheus-config-configmap.yaml
kubectl apply -f monitoring/prometheus/Azure\ prometheus/ama-metrics-settings-configmap-rabin.yaml
```

```bash
# Golem
kubectl diff -f monitoring/prometheus/Azure\ prometheus/ama-metrics-prometheus-config-configmap.yaml
kubectl diff -f monitoring/prometheus/Azure\ prometheus/ama-metrics-settings-configmap-golem.yaml
```

## FAQ
1) nikdy neodebirat label `job` nebo `instance`, protože pak se metriky nesbiraji
2) Logy jsou v namespace `kube-system` a pod s názvem `ama-metrics-<neco>-<neco>`


## Self managed Grafana
https://learn.microsoft.com/en-us/azure/azure-monitor/essentials/prometheus-grafana#self-managed-grafana

Auth se řeší přes APP registration kde jsem pouzil grafana-dev co se jiz pouziva pro monitor https://portal.azure.com/#view/Microsoft_AAD_RegisteredApps/ApplicationMenuBlade/~/Credentials/appId/525acc68-fbcb-4aa0-8b8c-4ee2e8d57601
