## Blackbox exporter (DEPRECATED)

prometheus-blackbox-exporter - web checky na API vehicle-positions (nahrazeno Zabbixem).
```bash
helm upgrade --install -n monitoring prometheus-blackbox-exporter prometheus-community/prometheus-blackbox-exporter --values monitoring/prometheus/blackbox-exporter/values.yaml
```
### Ropid
```bash
helm upgrade --install -n monitoring prometheus-blackbox-exporter prometheus-community/prometheus-blackbox-exporter --values monitoring/prometheus/blackbox-exporter/values-ropid.yaml
```
