# Kubernetes event exporter

https://github.com/resmoio/kubernetes-event-exporter

Kubernetes drží historii eventů pouze 1 hodinu. Exporter posílá eventy z clusteru do Elasticsearch (kube-events-<datum> indexy).

## Instalace
### Secrets
```bash
# Rabin
kubectl apply -f ../rabin/monitoring/kubernetes-event-exporter_01-secret.yaml

# Golem
kubectl apply -f ../golem/monitoring/kubernetes-event-exporter_01-secret.yaml
```

### Common
V Kibaně v Dev Tools aplikovat monitoring/prometheus/kubernetes-event-exporter/02-index-template.es.

```bash
kubectl apply -f monitoring/prometheus/kubernetes-event-exporter/00-roles.yaml
kubectl apply -f monitoring/prometheus/kubernetes-event-exporter/03-deployment.yaml
```