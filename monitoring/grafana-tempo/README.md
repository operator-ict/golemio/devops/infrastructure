# Grafana Tempo

https://github.com/grafana/helm-charts/tree/main/charts/tempo

# Tempo remotewrite
https://learn.microsoft.com/en-us/azure/azure-monitor/containers/prometheus-remote-write-managed-identity
Docker image also in link

```bash
helm repo add grafana https://grafana.github.io/helm-charts
helm repo update

# Rabin
kaf ../rabin/grafana-tempo/secret-azure.yaml
helm upgrade --install -n monitoring --values cluster_development/monitoring_grafana-tempo.yaml tempo grafana/tempo --version 1.10.3
kubectl apply -f monitoring/grafana-tempo/tempo-remotewrite.yaml

# Golem
kaf ../golem/grafana-tempo/secret-azure.yaml
helm upgrade --install -n monitoring --values cluster_production/monitoring_grafana-tempo.yaml tempo grafana/tempo --version 1.10.3
kubectl apply -f monitoring/grafana-tempo/tempo-remotewrite.yaml
```
