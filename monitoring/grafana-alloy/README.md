# Grafana Alloy

https://github.com/grafana/alloy/tree/main/operations/helm/charts/alloy

```bash
helm repo add grafana https://grafana.github.io/helm-charts
helm repo update

# Rabin
helm upgrade --install -n monitoring --values monitoring/grafana-alloy/common.yaml --values cluster_development/monitoring_grafana-alloy.yaml grafana-alloy grafana/alloy  --version 0.9.1

# Golem 
helm upgrade --install -n monitoring --values monitoring/grafana-alloy/common.yaml --values cluster_production/monitoring_grafana-alloy.yaml grafana-alloy grafana/alloy  --version 0.9.1
```
