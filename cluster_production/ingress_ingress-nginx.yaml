controller:
  config:
    # debug, info, notice, warn, error, crit, alert, or emerg.
    error-log-level: warn
    log-format-escape-json: "true"
    # Modified default log format converted to JSON string.
    log-format-upstream: >-
      {
      "StartLocal":"$time_iso8601",
      "ClientAddr":"$remote_addr",
      "XForwardedFor": "$proxy_add_x_forwarded_for",
      "RequestMethod": "$request_method",
      "RequestHost": "$host",
      "ServerName": "$server_name",
      "RequestPath": "$uri",
      "QueryString": "$query_string",
      "DownstreamStatus":"$status",
      "Duration":"$request_time",
      "DownstreamBytes": "$bytes_sent",
      "HttpUserAgent":"$http_user_agent",
      "RequestLength":"$request_length",
      "BackendAddr":"$upstream_addr",
      "OriginStatus":"$upstream_status",
      "OriginDuration":"$upstream_response_time",
      "OriginContentSize":"$upstream_response_length",
      "Encoding":"$sent_http_content_encoding"
      }
    # TCP and UDP logs
    log-format-stream: >-
      {
      "protocol": "$protocol",
      "status": "$status",
      "bytes_received": "$bytes_received",
      "addr": "$upstream_addr"
      }
    enable-opentelemetry: "true"
    opentelemetry-config: "/etc/nginx/opentelemetry.toml"
    opentelemetry-operation-name: "HTTP $request_method $service_name $uri"
    opentelemetry-trust-incoming-span: "false"
    otlp-collector-host: "tempo.monitoring.svc.cluster.local"
    otlp-collector-port: "4317"
    otel-max-queuesize: "2048"
    otel-schedule-delay-millis: "5000"
    otel-max-export-batch-size: "512"
    otel-service-name: "nginx-proxy" # Opentelemetry resource name
    otel-sampler: "TraceIdRatioBased" # Also: AlwaysOn, AlwaysOff, TraceIdRatioBased
    otel-sampler-ratio: "0.01"
    otel-sampler-parent-based: "false"
  opentelemetry:
    enabled: true
    resources:
      requests:
        cpu: 5m
        memory: 64Mi
      limits:
        cpu: 100m
        memory: 100Mi
  image:
    allowPrivilegeEscalation: true
  # The name of the ingress class this nginx instance will serve.
  ingressClass: nginx
  electionID: ingress-controller-leader
  # The name of the ingress class this helm chart will create.
  ingressClassResource:
    name: nginx
    default: true
  replicaCount: 2
  resources:
    requests:
      cpu: 50m
      memory: 320Mi
    limits:
      cpu: 150m
      memory: 750Mi
  service:
    annotations:
      service.beta.kubernetes.io/azure-load-balancer-resource-group: rg-public_ips-golem
      service.beta.kubernetes.io/azure-load-balancer-health-probe-request-path: /healthz
    # Set external traffic policy to: "Local" to preserve source IP on providers supporting it.
    # Ref: https://kubernetes.io/docs/tutorials/services/source-ip/#source-ip-for-services-with-typeloadbalancer
    externalTrafficPolicy: "Local"
    loadBalancerIP: 20.4.89.193
    sessionAffinity: "ClientIP"
  setAsDefaultIngress: true
  extraArgs:
    default-ssl-certificate: ingress/nginx.golemio.cz-cert
    time-buckets: "0.3,0.7,1.5,5,10"
    length-buckets: "1000,5000,10000,1e+05,1e+06,1e+07"
    size-buckets: "100,1000,10000,100000,1e+06,1e+07"
  allowSnippetAnnotations: true
  enableAnnotationValidations: true
  metrics:
    enabled: true
  affinity:
    nodeAffinity:
      requiredDuringSchedulingIgnoredDuringExecution:
        nodeSelectorTerms:
          - matchExpressions:
              - key: autoscale
                operator: NotIn
                values:
                  - "true"

tcp:
  3003: "golemio/data-proxy-data:3003"
  3004: "golemio/data-proxy-data:3004"
  3006: "golemio/data-proxy-data:3006"
  3008: "golemio/data-proxy-data:3008"
  10022: "golemio/sshd:22"
