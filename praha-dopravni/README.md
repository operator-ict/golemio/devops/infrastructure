
# Rabin
```bash
helm upgrade -n frontend --values ./praha-dopravni/values-development-versions.yaml --values cluster_development/frontend_praha-dopravni-values.yaml praha-dopravni ./praha-dopravni
```

# Golem
```bash
helm upgrade -n frontend --values ./praha-dopravni/values-master-versions.yaml --values cluster_production/frontend_praha-dopravni-values.yaml praha-dopravni ./praha-dopravni
```
