{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "praha-dopravni.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "praha-dopravni.fullname" -}}
{{- if .Values.fullnameOverride -}}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- $name := default .Chart.Name .Values.nameOverride -}}
{{- if contains $name .Release.Name -}}
{{- .Release.Name | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{- end -}}
{{- end -}}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "praha-dopravni.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Selector labels
*/}}
{{- define "praha-dopravni.selectorLabels" -}}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end -}}

{{/*
Common labels
*/}}
{{- define "praha-dopravni.labels" -}}
{{- $top := index . 0 -}}
{{- $appSection := index . 1 -}}
{{ include "praha-dopravni.selectorLabels" $top }}
helm.sh/chart: {{ include "praha-dopravni.chart" $top }}
app.kubernetes.io/managed-by: {{ $top.Release.Service }}
app.kubernetes.io/part-of: {{ include "praha-dopravni.name" $top }}
{{- if $top.Chart.AppVersion }}
app.kubernetes.io/version: {{ $top.Chart.AppVersion | quote }}
{{- end }}
{{- if (not (empty (index $top.Values $appSection "labels"))) }}
{{ (index $top.Values $appSection "labels") | toYaml }}
{{- end }}
{{- end -}}

{{/*
Common annotations
*/}}
{{- define "praha-dopravni.annotations" -}}
{{- end -}}

{{/*
Custom helpers
*/}}

{{- define "praha-dopravni.imagePullSecrets" -}}
  {{- $top := index . 0 -}}
  {{- $pullSecrets := index . 1 -}}

  {{- if (not (empty $pullSecrets)) }}
imagePullSecrets:
    {{- range $pullSecrets | uniq }}
  - name: {{ . }}
    {{- end }}
  {{- end }}
{{- end -}}

{{- define "praha-dopravni.imageName" -}}
{{- $top := index . 0 -}}
{{- $appSection := index . 1 -}}
{{ index $top "Values" "global" "imageRegistry" }}/{{ index $top "Values" $appSection "repository" }}{{ if index $top "Values" $appSection "branch" }}/{{ index $top "Values" $appSection "branch" }}{{- end -}}:{{ index $top "Values" $appSection "tag" }}
{{- end -}}

{{- define "praha-dopravni.readinessProbe" -}}
{{- $top := index . 0 -}}
{{- $appSection := index . 1 -}}
{{- $ingress := index $top "Values" $appSection "ingress" | default dict -}}
httpGet:
  path: {{ index $ingress "path" | default "/" | quote }}
  port: {{ index $top "Values" $appSection "port" }}
initialDelaySeconds: {{ index $top "Values" "global" "readinessProbe" "initialDelaySeconds" }}
periodSeconds: {{ index $top "Values" "global" "readinessProbe" "periodSeconds" }}
{{- end -}}

{{- define "praha-dopravni.livenessProbe" -}}
{{- $top := index . 0 -}}
{{- $appSection := index . 1 -}}
{{- $ingress := index $top "Values" $appSection "ingress" | default dict -}}
httpGet:
  path: {{ index $ingress "path" | default "/" | quote }}
  port: {{ index $top "Values" $appSection "port" }}
{{- end -}}
