# Začínáme

## Instalace
Postup jak [spustit celý projekt](docs/getting-started.md).

Pro využití k8S secrets si stáhnete repositare do stejného adresáře jako toto repo pod názvy `rabin` a `golem`.

```bash
./infrastructure
./rabin
./golem
```

# Release
[Vydání nové major verze API](docs/major-release.md)

# Kubernetes

Konfigurace [kubernetes](https://gitlab.com/operator-ict/devops/kubernetes/-/blob/master/README.md).

# Jak pustit pipeline?
Na adrese https://gitlab.com/operator-ict/golemio/devops/infrastructure/-/pipelines kliknout na `run pipeline` a pak zadat proměnné dle potřeby, například:
```bash
HELM_NAME = golemio
K8S_CLUSTER = development
```

## Stage

```bash
UPSTREAM_BRANCH = stage
HELM_NAME = covid
```

## Produkce

```bash
HELM_NAME = golemio
K8S_CLUSTER = production
```

# K8S taháky

## Jak otočit pody pod změně secrets
```bash
kubectl rollout restart deployment <name_of_deployment>
```


# Ansible 
```
ansible-playbook playbooks/postgres-users.yml -i inventories/golem.yml --tags=psql -l monitor
```
