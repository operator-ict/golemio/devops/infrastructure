# Frontdoor

## FAQ

### Nový origin nefunguje
FD přistupuje na origin často přes IP a ta nemá žádný certifikát, tak je potřeba dodat platný certifikat s libovolnou domenou jako defaultní do nginx ingress.

## Debug

V případě že domena hází chybu 502 lze zjistit příčinu pomocí 
```bash
curl -IL https://mojedomena.cz -H 'X-Azure-DebugInfo: 1'

HTTP/2 502 
date: Fri, 04 Oct 2024 08:58:00 GMT
content-type: text/html
content-length: 1379
cache-control: no-store
x-azure-ref: 20241004T085800Z-1588498f885zrht2k0e4adaz2800000001a000000000u91h
x-azure-originstatuscode: 502
x-azure-externalerror: OriginCertificateSelfSigned
x-cache: CONFIG_NOCACHE
```