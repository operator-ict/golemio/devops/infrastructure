
# Flow logs
Je potreba zapnout i sběr analytik viz https://thesleepyadmins.com/2023/01/10/azure-network-watcher-nsg-flow-logs-review-nsg-traffic/

```kql
AzureNetworkAnalytics_CL
| where DestPort_d == 3004 and FlowDirection_s == "I"

AzureNetworkAnalytics_CL
| where DestPort_d in (3003, 3004, 3006, 3008) and FlowDirection_s == "I" and DestIP_s == "20.4.89.193"
| summarize Count = sum(FlowCount_d) by SrcIP_s, DestPort_d
| sort by SrcIP_s, DestPort_d asc
```