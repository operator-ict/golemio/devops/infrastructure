# API Gateway

- [Doporučení Alibaba](https://www.alibabacloud.com/knowledge/hot/best-open-source-api-gateways-to-consider)

## Co hledáme?
- neplatí se dle počtu requestů, protože těch máme mnoho
- jak vyřešit elegatně přihlašování (nyní JWT s ověřením u třetí strany) - "Token Introspection"
- ideálně rozšiřitelné (pluginy) přes JS/TS
- zatím jsme neřešili OAuth jako součást pohledávky

Současný stav:
![](img/api_gateway_current_state.png)

<!---
# Název
- URL:
- Opensource:

### Poznámky
- @todo 

### Výhody

### Nevýhody
-->

## Ory
- URL: https://www.ory.sh/open-source/
- Opensource: Ano, nenašel jsem co by open source nepodporoval oproti placené verzi.

![API Gateway Ory Oathkeeper - High level](img/api_gateway_oathkeeper.png)

### Poznámky
- Rozdělené na několik menších služeb. 
  - [oathkeeper](https://www.ory.sh/docs/oathkeeper) je jako naše Access Proxy + routování z PP managementu    
    - vidím potenciál ve využití authorizace [remote](https://www.ory.sh/docs/oathkeeper/pipeline/authz#remote) v kombinaci se stávajícím řešením našeho PP managementu    
    - nenašel jsem, jak nahradit funkčnost našich limitací možná by část šlo pokrýt pomocí doplnění custom hlaviček viz [hydrator](https://www.ory.sh/docs/oathkeeper/pipeline/mutator#hydrator-configuration)
  - [kratos](https://www.ory.sh/docs/kratos/ory-kratos-intro) je user management něco jako PP management
    - `remote` endpoint (pro authorizaci) a tool pro generování json tokenů je třeba napsat

### Výhody
- Není to Java
- Je to mikroservisní architektura
- Podobné tomu na co jsme zvyklí
- Připravené pro cloud (helm charts)

### Nevýhody
- Je potřeba si nastavit vlastní GUI dle připravených šablon, aby to zapadalo do našeho workflow. 
- navíc další turn around time pro autorizaci uživatele např. přes  Kratos nebo vlastním endpointem (remote authorizace)
- nenašel jsem jak vytvořit custom plugin

### Závěry z používání prototypu

**kratos**

- je production ready
- nabízí hodně providerů
  - social sign in, azure ad
  - až 3-fázové ověření, a dá se podle toho řídit přístup
- registrace, zapomenuté heslo, ověření emailu atd. jsme teď netestovali, ale stačí to povolit
- vylistování uživatelů řeší admin api, bude volat gbi (nebo konkrétní web)
- co vyřešit
  - zabezpečený přístup k admin api
    - v ne cloud verzi je to prostě otevřený, takže je potřeba vymyslet způsob, jak to zavřít. Možná bude stačit basic auth na úrovni ingressu
- otázky
  - jak flexibilní jsou šablony pro emaily?
    - emaily chodí správně, ale netestovali jsme jejich customizaci

**keto**

- ? není production ready
- je to ale dost jednoduchý, dá se nahradit vlastní implementací (cca 1 sprint)
- dá se nastavit cokoliv přes schéma
- co vyřešit
  - vyřešit nějak inicializaci databáze, u vymi bylo uděláno ručně

**oathkeeper**

- zatím jsme víc neřešili, pro použití pro weby to neni potřeba, znovu analyzujeme pro použití pro api gateway
- slabá stránka: reload pravidel


## Kong
- URL: https://konghq.com/
- Opensource: Ano, ale některé [pluginy jsou placené](https://docs.konghq.com/hub/?tier=paid%2Cpremium%2Centerprise).

### Poznámky
- @todo [GUI](https://docs.konghq.com/gateway/3.5.x/kong-manager-oss/) má verzi OSS, ale je potřeba vyzkoušet její omezení
- [srovnání verzí pro self-managed](https://docs.konghq.com/gateway/3.5.x/#self-managed)
- @toto ověřit jak ověřovat uživatele po zaslání JWT ještě proti DB

### Výhody
- výkonná API GW
- enterprise ověřené řešení

### Nevýhody
- není veřejný [ceník](https://konghq.com/pricing)
- vlastní pluginy jsou v [Lua](https://docs.konghq.com/gateway/latest/plugin-development/custom-logic/)


## Tyk
- URL: https://tyk.io/
- Opensource: Ano, [GUI není opensource](https://tyk.io/docs/apim/open-source/#tyk-open-source)

![API Gateway Tyk - High level](img/api_gateway_tyk.png)

### Poznámky
- [Srovnání s konkurencí](https://tyk.io/comparison/)
- řešení limitací validační část:
  - [validate request docs](https://tyk.io/docs/product-stack/tyk-gateway/middleware/validate-request-tyk-oas/)
- řešení limitací přepisující/dopisující parametr:
  - url rewrite middleware mi přijde nedostatečný
  - obdobně jako v případě ory bychom se mohli přeorientovat na custom header
  - ? nejsem si jist jak toto řešit
- @toto ověřit jak ověřovat uživatele po zaslání JWT ještě proti DB
  - teoreticky lze vytvořit post authentication plugin viz [auth plugins docs](https://tyk.io/docs/plugins/plugin-types/auth-plugins/auth-plugins/)
  - případně 
- pro zjenodušení vytvoření nových APIs lze použít templatovací systém 
- přijde mi to docela jako velké kladivo na naše potřeby (@MP)

### Výhody
- výkonná API GW (o něco méně než Kong)
- enterprise ověřené řešení
- pluginy nejsou zpoplatněny zvlášt jako u Kong
- cena je fixní, nezávisí na počtu requestů

### Nevýhody
- [GUI není open source](https://tyk.io/docs/tyk-stack/)
- neveřejný [ceník pro self-managed](https://tyk.io/pricing-self-managed/)
- custom pluginy mimo náš stack (Go, gRPC, Lua, OTTO, Python)

## Azure API Management
- URL: https://learn.microsoft.com/en-us/azure/api-management/api-management-key-concepts
- Opensource: Ne
### Poznámky
- @todo
### Výhody
- výkonná API GW
- enterprise ověřené řešení
- více možností jak počítat cenu
- je součástí Azure

### Nevýhody
- nelze připojit 3rd party na doupřesnění opravnění => nutné změnit logiku ověřování
- není přenositelné mimo Azure 


## KrakenD
- URL: https://www.krakend.io/
- Opensource: Ano, některé pluginy jsou jen v Enterprise
- Demo: https://designer.krakend.io/#!/service

### Poznámky
- @toto ověřit jak ověřovat uživatele po zaslání [JWT ještě proti DB](https://www.krakend.io/docs/authorization/jwt-validation/) - přístup na DB by mohl řešit [Key Cloak](https://www.krakend.io/docs/authorization/keycloak/)
- [srovnání Community a Enterprise](https://www.krakend.io/features/)
-  dle emailu (KrakenD Enterprise information for Operator ICT | ze dne 14.2.2024) začínají na 12,K Euro/rok pro Starter
- @todo určitě ještě více prozkoumat

### Výhody
- GUI je open-source
- [vysoce výkonná API GW](https://www.krakend.io/docs/benchmarks/api-gateway-benchmark/#requests-per-second)
- Stateless (nemá databázi), tedy dobře škálovatelná a nezávislá

### Nevýhody
- některé pluginy jsou v Enterprise 
  - [API–keys](https://www.krakend.io/docs/enterprise/authentication/api-keys/) - hodně podobné naší aktuální verzi přihlšování
  - [Rate limit](https://www.krakend.io/docs/enterprise/throttling/global-rate-limit/)
  - [GZip](https://www.krakend.io/docs/enterprise/service-settings/gzip/) - lze řešit asi na ingressu
- vlastní pluginy pouze v [Lua a Go](https://www.krakend.io/docs/extending/)


# Gravitee
- URL: https://gravitee.io
- Opensource: [Ano](https://documentation.gravitee.io/platform-overview/gravitee-essentials/gravitee-offerings-ce-vs-ee)
- Demo: [lokální Docker](https://docs.gravitee.io/apim/3.x/apim_installation_guide_docker_compose_quickstart.html)

### Poznámky
- @todo zjistit co všechno nám chybí z Enterprise
- @todo určitě ještě více prozkoumat sluzby [AM](https://documentation.gravitee.io/am/getting-started/install-and-upgrade-guides/run-in-docker/docker-compose-install) (admin /adminadmin) a [APIM](https://documentation.gravitee.io/apim/getting-started/install-guides/install-on-docker/custom-install-with-docker-compose)
### Výhody
-

### Nevýhody
- [vlastní role pouze v Enterprise](https://documentation.gravitee.io/apim/guides/administration/user-management-and-permissions#roles)
- složitější struktura komponent
- pracuje to s Elasticsearch, který nemáme, ale není nezbytný
- pracuje s Mongo a mělo by umět i PSQL, ale v demo Dockeru to není
- běží na Java


# Gloo Edge
- URL: https://docs.solo.io/gloo-edge/latest/
- Opensource: Ano

### Poznámky
- @todo zatím jsem nestihl prozkoumat, ale stojí to za to
- Gloo Gateway (základní, ale taky prozkoumat) je podmnožinou Gloo Edge

### Výhody
- Kubernetes native

### Nevýhody
- GUI je pouze v enterprise
- neveřejný ceník


## Fusio
- URL: https://www.fusio-project.org
- Opensource: Ano
- Demo: https://www.fusio-project.org/demo

### Poznámky

### Výhody
- psané v JS/TS
- plně open source
- rozšiřitelná logika zpracování requestů přes různé actions

### Nevýhody
- nízká [aktivita vývoje](https://github.com/apioo/fusio/graphs/commit-activity) a počet [contributorů](https://github.com/apioo/fusio/graphs/contributors), ale má 1.7K github stars
- nejasná výkonnost


# WSO2 API Microgateway
- URL: https://mg.docs.wso2.com/en/latest/
- Opensource: Ano

### Poznámky
- @todo nezkoumal jsem vůbec

### Výhody


### Nevýhody



# API Umbrella
- URL: https://github.com/NREL/api-umbrella/tree/main
- Opensource: Ano

### Poznámky
- @todo moc jsem nezkoumal, ale vypadá to neudržovaně

### Výhody
- používá https://api.data.gov/

### Nevýhody



# Ostatní
- [Apiman](https://github.com/apiman/apiman) - minimální vývoj, běží na Javě
- [Vulcand](https://github.com/vulcand/vulcand/) - žádný vývoj několik let
- [Goku API Gateway](https://github.com/eolinker/goku_lite) - žádný vývoj několik let
- [APache APISIX](https://apisix.apache.org/) - zkoumal Tom a nevyhovovalo to, [analýza v nodejs chapter](https://gitlab.com/operator-ict/oddeleni-vyvoje/extras/nodejs-chapter/-/blob/main/tasks/permission-proxy/apisix_analyza_2023-07-28.md?ref_type=heads)
