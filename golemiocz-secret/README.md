# Rabin
```bash
helm upgrade --install --namespace frontend --values ../rabin/golemiocz/values-secret.yaml golemiocz-secret golemiocz-secret
```

# Golem
```bash
helm upgrade --install --namespace frontend --values ../golem/golemiocz/values-secret.yaml golemiocz-secret golemiocz-secret
```
