{{ $appSection := "tcp-input-gateway" -}}
{{- $appValues := index . "Values" $appSection -}}
{{ $appName := (index . "Values" $appSection "name") -}}
{{- if index . "Values" $appSection "enabled" -}}
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ $appName }}
  labels:
    app.kubernetes.io/name: {{ $appName }}
    {{- include "golemio.labels" (list . $appSection) | nindent 4 }}
  {{- if .Values.commonAnnotations }}
  annotations: {{- .Values.commonAnnotations | toYaml | nindent 4 }}
  {{- end }}
spec:
  replicas: {{ index .Values $appSection "replicas" }}
  strategy: {{- include "golemio.rollingUpdate" (index . "Values" $appSection) | nindent 4 }}
  selector:
    matchLabels:
      app.kubernetes.io/name: {{ $appName }}
      {{- include "golemio.selectorLabels" . | nindent 6 }}
  template:
    metadata:
      labels:
        app.kubernetes.io/name: {{ $appName }}
        {{- include "golemio.labels" (list . $appSection) | nindent 8 }}
      {{- if .Values.commonAnnotations }}
      annotations: {{- .Values.commonAnnotations | toYaml | nindent 8 }}
      {{- end }}
    spec:
      automountServiceAccountToken: false
      containers:
        - name: {{ $appName }}
          image: {{ include "golemio.imageName" (list . $appSection) | quote }}
          imagePullPolicy: {{ .Values.global.pullPolicy }}
          ports:
            - containerPort: {{ index .Values $appSection "tram" "port" }}
            - containerPort: {{ index .Values $appSection "bus" "port" }}
            - containerPort: {{ index .Values $appSection "metro" "port" }}
          env:
            - name: APP_NAME
              value: {{ index .Values $appSection "name" }}
            - name: LOG_LEVEL
              value: {{ index .Values $appSection "logLevel" }}
            - name: NODE_ENV
              value: {{ index .Values $appSection "nodeEnv" }}
            - name: NODE_OPTIONS
              value: {{ include "golemio.maxOldSpaceSize" (index . "Values" $appSection "resources" "limits" "memory" ) | quote}}
            - name: DPP_TRAM_RECEIVER_PORT
              value: {{ index .Values $appSection "tram" "port" | quote }}
            - name: DPP_METRO_RECEIVER_PORT
              value: {{ index .Values $appSection "metro" "port" | quote }}
            - name: DPP_BUS_RECEIVER_PORT
              value: {{ index .Values $appSection "bus" "port" | quote }}
            - name: ARRIVA_CITY_RECEIVER_PORT
              value: {{ index .Values $appSection "arriva" "port" | quote }}
            - name: RABBIT_CONN
              valueFrom:
                secretKeyRef:
                  name: {{ .Values.global.secretName }}
                  key: inputGatewayRabbitConn
            - name: RABBIT_EXCHANGE_NAME
              value: {{ index .Values $appSection "rabbit" "exchangeName" }}
            - name: STORAGE_ENABLED
              value: {{ index .Values $appSection "azure" "enabled" | toString | quote }}
            {{- if eq (index .Values $appSection "azure" "enabled" | toString) "true" }}
            - name: AZURE_BLOB_ACCOUNT
              value: {{ index .Values $appSection "azure" "account" | quote }}
            - name: AZURE_BLOB_TENANT_ID
              valueFrom:
                secretKeyRef:
                  name: {{ .Values.global.secretName }}
                  key: azureTableStorageIGTenantId
            - name: AZURE_BLOB_CLIENT_ID
              valueFrom:
                secretKeyRef:
                  name: {{ .Values.global.secretName }}
                  key: azureTableStorageIGClientId
            - name: AZURE_BLOB_CLIENT_SECRET
              valueFrom:
                secretKeyRef:
                  name: {{ .Values.global.secretName }}
                  key: azureTableStorageIGClientSecret
            - name: AZURE_BLOB_STORAGE_CONTAINER_NAME
              value: {{ index .Values $appSection "azure" "container" | quote }}
            {{- end }}
            - name: TABLE_STORAGE_ENABLED
              value: {{ index .Values $appSection "azureTableStorage" "enabled" | toString | quote }}
            {{- if eq (index .Values $appSection "azureTableStorage" "enabled" | toString) "true" }}
            - name: AZURE_TABLE_ACCOUNT
              value: {{ index .Values $appSection "azureTableStorage" "account" | quote }}
            - name: AZURE_TABLE_CLIENT_ID
              valueFrom:
                secretKeyRef:
                  name: {{ .Values.global.secretName }}
                  key: azureTableStorageIGClientId
            - name: AZURE_TABLE_TENANT_ID
              valueFrom:
                secretKeyRef:
                  name: {{ .Values.global.secretName }}
                  key: azureTableStorageIGTenantId
            - name: AZURE_TABLE_CLIENT_SECRET
              valueFrom:
                secretKeyRef:
                  name: {{ .Values.global.secretName }}
                  key: azureTableStorageIGClientSecret
            - name: AZURE_TABLE_STORAGE_ENTITY_BATCH_SIZE
              value: {{ index .Values $appSection "azureTableStorage" "entityBatchSize" | quote }}
            {{- end }}
            - name: JAEGER_ENABLED
              value: {{ index .Values $appSection "opentelemetry" "enabled" | quote }}
            {{- if index .Values $appSection "opentelemetry" "enabled" }}
            - name: JAEGER_PROCESSOR
              value: {{ index .Values.opentelemetry.processor | upper | quote }}
            - name: JAEGER_AGENT_HOST
              value: {{ index .Values.opentelemetry.host | quote }}
            - name: JAEGER_AGENT_PORT
              value: {{ index .Values.opentelemetry.port | quote }}
            - name: OTEL_TRACES_SAMPLER
              value: {{ index .Values.opentelemetry.sampler | quote }}
            - name: OTEL_TRACES_SAMPLER_ARG
              value: {{ index .Values $appSection "opentelemetry" "samplerArg" | quote }}
            - name: TELEMETRY_LOG_LEVEL
              value: {{ index .Values.opentelemetry.logLevel | quote }}
            {{- end }}
          resources:
            {{ index .Values $appSection "resources" | toYaml | nindent 12 }}
          securityContext:
            allowPrivilegeEscalation: false
            runAsNonRoot: true
            runAsUser: 1001
            capabilities:
              drop: [ "all" ]
          {{- if index . "Values" $appSection "probes" "enabled" }}
          readinessProbe: {{ index .Values $appSection "readinessProbe" | toYaml | nindent 12 }}
          livenessProbe: {{ index .Values $appSection "livenessProbe" | toYaml | nindent 12 }}
          {{ end }}
      tolerations:
        {{ index . "Values" $appSection "tolerations" | toYaml | nindent 8 }}
      nodeSelector:
        {{ index . "Values" $appSection "nodeSelector" | toYaml | nindent 8 }}
{{ end }}
