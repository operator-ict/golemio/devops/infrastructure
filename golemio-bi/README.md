
# Rabin
```bash
helm upgrade -n frontend --values ./golemio-bi/values-development-versions.yaml --values cluster_development/frontend_golemio-bi-values.yaml golemio-bi ./golemio-bi
```

# Golem
```bash
helm upgrade -n frontend --values ./golemio-bi/values-master-versions.yaml --values cluster_production/frontend_golemio-bi-values.yaml golemio-bi ./golemio-bi
```
