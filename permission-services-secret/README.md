# Rabin
```bash
helm upgrade --install -n permission-services --values ../rabin/permission-services/values-secret.yaml permission-services-secret ./permission-services-secret
```

# Rabin / mapk2o
```bash
helm upgrade --install -n mapk2o --values ../rabin/mapk2o-permission-services/values-secret.yaml permission-services-secret ./permission-services-secret
```


# Golem
```bash
helm upgrade --install -n permission-services --values ../golem/permission-services/values-secret.yaml permission-services-secret ./permission-services-secret
```

# Golem / mapk2o
```bash
helm upgrade --install -n mapk2o --values ../golem/mapk2o-permission-services/values-secret.yaml permission-services-secret ./permission-services-secret
```
