# Rabin
```bash
helm upgrade --install --namespace jis --values ../rabin/vymi-alerts/values-secret.yaml  vymi-alerts-secret vymi-alerts-secret
```

# Golem
```bash
helm upgrade --install --namespace jis --values ../golem/vymi-alerts/values-secret.yaml  vymi-alerts-secret vymi-alerts-secret
```
