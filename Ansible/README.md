# Ansible

## Install on Mac M1/Ubuntu WSL

https://github.com/ansible-collections/azure#installation

Na MacOS instalovat cely ansible pres pip a nikoliv pres Brew

```bash
pip install psycopg2-binary
ansible-galaxy collection install azure.azcollection
pip install -r ~/.ansible/collections/ansible_collections/azure/azcollection/requirements-azure.txt
```

## Run postgres playbook
```bash
ansible-playbook Ansible/postgres-users.yml -i ../golem/ansible/golem.yml --tag keyvault,psql
```

## Create pureFTP server
```bash
ansible-playbook playbooks/pureftp.yml -i inventories/pureftp_rabin.yml --ask-vault-pass
OR
ansible-playbook playbooks/pureftp.yml -i inventories/pureftp_rabin.yml --vault-password-file Security/Golemio-Rabín/ansible/vaultpass.txt
```

## Using playbook for Grafana dashboard
```bash
ansible-playbook Ansible/playbooks/grafana.yml -i Ansible/inventories/grafana.yml --ask-vault-pass
OR
ansible-playbook Ansible/playbooks/grafana.yml -i Ansible/inventories/grafana.yml  --vault-password-file ../golem/ansible/vaultpass.txt

Default direction PROD -> DEV 
to change direction
ansible-playbook Ansible/playbooks/grafana.yml -i Ansible/inventories/grafana.yml --extra-vars 'direction=dev2prod' --ask-vault-pass
dasboards and directories to sync are set in Ansible/inventories/grafana.yml
```

## Debugging

### Nelze spustit Ansible - Azure
https://stackoverflow.com/questions/46700157/importerror-cannot-import-name-blobservice-when-using-azure-backend
```bash
The error was: ImportError: cannot import name 'PageBlobService' from 'azure.storage.blob'
```

```bash
# 1. odebrat azure-storage-blob
pip uninstall azure-storage-blob

# 2. nainstalovat starší balíček azure-storage 
pip install azure-storage==0.36.0
```

```bash
#RABIN
ansible-playbook Ansible/postgres-users.yml -i ../rabin/ansible/rabin.yml --tag keyvault,psql

#GOLEM
ansible-playbook Ansible/postgres-users.yml -i ../golem/ansible/golem.yml --tag keyvault,psql  
```
