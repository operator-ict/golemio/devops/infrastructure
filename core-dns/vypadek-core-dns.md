# Výpadek core dns

U výpadku jsme pouzili pro nefunční domény vlastní DNS server

```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: coredns-custom
  namespace: kube-system
data:
  test.server: | # you may select any name here, but it must end with the .server file extension
    apicore.zpspraha.cz:53 {
        forward apicore.zpspraha.cz 8.8.8.8
    }

    www.tsk-praha.cz:53 {
        forward www.tsk-praha.cz 8.8.8.8
    }

    tsk-praha.cz:53 {
        forward tsk-praha.cz 8.8.8.8
    }


```