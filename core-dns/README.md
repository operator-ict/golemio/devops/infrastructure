# Core DNS

## Lokální doména k8s-local
Pro nastavení domény dostupné pouze v rámci K8s clusteru a obsluhované pomocí nginx-ingress. 

1. Spustit nginx-ingress, který je bude obsluhovat, aby byla jasná IP na kterou se to má nasměrovat.
2. Nastavit Core DNS pomocí souboru `kubectl apply -f <prostřední>-dns-k8s-local.yaml`
3. Nastavit NSG pro AKS aby daná IP nebyla zvenku dostupná.


 