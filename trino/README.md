# Trino

https://github.com/trinodb/charts


## Rabin
```
helm upgrade --install -n tmp trino trino/trino --values ./trino/values-dev.yaml --version 0.30.0
```


## Ruční inicializace tabulek
https://github.com/trinodb/trino/issues/20419
```
https://github.com/trinodb/trino/blob/444/testing/trino-product-tests-launcher/src/main/resources/docker/presto-product-tests/conf/environment/singlenode-spark-iceberg-jdbc-catalog/create-table.sql#L1-L20
```