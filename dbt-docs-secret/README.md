# Rabin
```bash
helm upgrade --install -n prefect --values ../rabin/dbt-docs/values-secret.yaml dbt-docs-secret ./dbt-docs-secret
```

# Golem
```bash
helm upgrade --install -n prefect --values ../golem/dbt-docs/values-secret.yaml dbt-docs-secret ./dbt-docs-secret
```
