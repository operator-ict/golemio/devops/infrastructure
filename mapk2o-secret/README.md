# Rabin
```bash
helm upgrade --install --namespace mapk2o --values ../rabin/mapk2o/values-secret.yaml  mapk2o-secret mapk2o-secret
```

# Golem
```bash
helm upgrade --install --namespace mapk2o --values ../golem/mapk2o/values-secret.yaml  mapk2o-secret mapk2o-secret
```
