{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "dbt-docs.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "dbt-docs.fullname" -}}
{{- if .Values.fullnameOverride -}}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- $name := default .Chart.Name .Values.nameOverride -}}
{{- if contains $name .Release.Name -}}
{{- .Release.Name | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{- end -}}
{{- end -}}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "dbt-docs.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Selector labels
*/}}
{{- define "dbt-docs.selectorLabels" -}}
app.kubernetes.io/instance: {{ .Release.Name }}
app.kubernetes.io/part-of: {{ include "dbt-docs.name" . }}
{{- end -}}

{{/*
Common labels
*/}}
{{- define "dbt-docs.labels" -}}
{{ include "dbt-docs.selectorLabels" . }}
helm.sh/chart: {{ include "dbt-docs.chart" . }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
{{- end -}}

{{/*
Common annotations
*/}}
{{- define "dbt-docs.annotations" -}}
{{- end -}}

{{/*
Create the name of the service account to use
*/}}
{{- define "dbt-docs.serviceAccountName" -}}
{{- if .Values.serviceAccount.create -}}
    {{ default (include "dbt-docs.fullname" .) .Values.serviceAccount.name }}
{{- else -}}
    {{ default "default" .Values.serviceAccount.name }}
{{- end -}}
{{- end -}}

{{/*
Custom helpers
*/}}

{{- define "dbt-docs.wft-parameters" -}}
{{- range $k, $v := . -}}
- name: {{ $k | quote }}
  value: {{ $v | quote }}
{{ end -}}
{{- end -}}

{{- define "dbt-docs.imagePullSecrets" -}}
  {{- $top := index . 0 -}}
  {{- $pullSecrets := index . 1 -}}

  {{- if (not (empty $pullSecrets)) }}
imagePullSecrets:
    {{- range $pullSecrets | uniq }}
  - name: {{ . }}
    {{- end }}
  {{- end }}
{{- end -}}

{{- define "dbt-docs.imageName" -}}
{{- $top := index . 0 -}}
{{- $appSection := index . 1 -}}
{{ index $top "Values" "global" "imageRegistry" }}/{{ index $top "Values" $appSection "repository" }}{{ if index $top "Values" $appSection "branch" }}/{{ index $top "Values" $appSection "branch" }}{{- end -}}:{{ index $top "Values" $appSection "tag" }}
{{- end -}}
