# DBT-DOCS

## Deploy configurations:

Deploy configs Rabin:
```
helm upgrade --install -n prefect --values ../rabin/dbt-docs/values-secret.yaml dbt-docs-secret ./dbt-docs-secret
helm upgrade --install -n prefect --values ./dbt-docs/values-development-versions.yaml --values cluster_development/prefect_dbt-docs-values.yaml dbt-docs ./dbt-docs
```

Deploy configs Golem:
```
helm upgrade --install -n prefect --values ../golem/dbt-docs/values-secret.yaml dbt-docs-secret ./dbt-docs-secret
helm upgrade --install -n prefect --values ./dbt-docs/values-master-versions.yaml --values cluster_production/prefect_dbt-docs-values.yaml dbt-docs ./dbt-docs
```
