# Prefect

https://github.com/PrefectHQ/prefect-helm/tree/main/charts/prefect-server
https://github.com/PrefectHQ/prefect-helm/tree/main/charts/prefect-worker

```bash
helm repo add prefect https://prefecthq.github.io/prefect-helm

```

# Rabin
```bash
# Server
helm upgrade --install -n prefect --values cluster_development/prefect_server-values.yaml prefect-server prefect/prefect-server

# Worker
helm upgrade --install -n prefect --values cluster_development/prefect_worker-values.yaml  prefect-worker prefect/prefect-worker
# Worker job tempalte update
prefect config set PREFECT_API_URL="http://prefect.rabin.oictinternal.cz/api"
prefect work-pool update --base-job-template base-job-template.json k8s-worker
```

# Golem
```bash
# Server
helm upgrade --install -n prefect --values cluster_production/prefect_server-values.yaml prefect-server prefect/prefect-server

# Worker
helm upgrade --install -n prefect --values cluster_production/prefect_worker-values.yaml  prefect-worker prefect/prefect-worker
# Worker job tempalte update
prefect config set PREFECT_API_URL="http://prefect.golem.oictinternal.cz/api"
prefect work-pool update --base-job-template base-job-template.json k8s-worker
```
