## Installing pureFTP on virtual machine

### Crerating virtual machine
terrafrom create virtual machine

Example:
```bash
virtual_machines:
  ftpserver:
    name: ftpserver
    subnet_id: ftp-dp-rabin
    sku: "Standard_A1_v2"
    data_disk_size: 36 # disk data by default system disk is 32GB
    public_ip: "ftp-dp-rabin-public" # public_ip have to be defined in config
    security_rules: # ports that should be open to internet
      allow-ftp: { priority: 190, destination_port_ranges: [ "16421" ] }
      allow-http: { priority: 200, destination_port_ranges: [ "80", "443" ] }
      allow-ftp-passive: { priority: 210, destination_port_ranges: [ "30000-30049" ] }
```

### Prepare system on virtual machines
Ansible playbook [**system.yml**](../Ansible/playbooks/system.yml)
- Create users and add to sudo
- Install system tools not installed by default

### Configure and format data disk
Ansible playbook  [**initialize-new-disks.yaml**](../Ansible/playbooks/initialize-new-disks.yml)
note: have to check name of disk is correct expecting sdc if not change accordingly

### Install and configure pureFTP
Ansible playbook [**pureftp.yml**](../Ansible/playbooks/pureftp.yml) (using vault secrets)
- Install docker
- Install and configure certbot for creating certificate
- Install and configure nginx
- Install and configure pureftp

### PureFTP managment
Connect to docker container
```bash
docker exec -it pureftpd /bin/sh
```

list users
```bash
pure-pw list
```

create new user
```bash
pure-pw useradd user_name -u uid(use 1100 for all) -g group_id(use 1100 for all) -d user_home_dir -m

Example:
pure-pw useradd tsk-parking-zones -u 1100 -g 1100 -d /home/external/tsk/parking-zones -m
```

delete user
```bash
pure-pw userdel user_name

Example:
pure-pw userdel ipt-prod
```
