# Ingress NGINX

# Rabin
```bash
helm upgrade --install -n ingress --values "cluster_development/ingress_ingress-nginx.yaml"          --version 4.10.0 ingress-nginx          ingress-nginx/ingress-nginx
helm upgrade --install -n ingress --values "cluster_development/ingress_ingress-nginx-internal.yaml" --version 4.10.0 ingress-nginx-internal ingress-nginx/ingress-nginx
helm upgrade --install -n ingress --values "cluster_development/ingress_ingress-nginx-local.yaml"    --version 4.10.0 ingress-nginx-local    ingress-nginx/ingress-nginx
```

# Golem
```bash
helm upgrade --install -n ingress --values "cluster_production/ingress_ingress-nginx.yaml"          --version 4.10.0 ingress-nginx          ingress-nginx/ingress-nginx
helm upgrade --install -n ingress --values "cluster_production/ingress_ingress-nginx-internal.yaml" --version 4.10.0 ingress-nginx-internal ingress-nginx/ingress-nginx
helm upgrade --install -n ingress --values "cluster_production/ingress_ingress-nginx-local.yaml"    --version 4.10.0 ingress-nginx-local    ingress-nginx/ingress-nginx
```

# Secrets

## Rabin
```bash
helm upgrade --install --namespace ingress --values ../rabin/ingress-nginx/values-secret.yaml ingress-nginx-secret ./ingress-nginx-secret
```

## Golem
```bash
helm upgrade --install  --namespace ingress --values ../golem/ingress-nginx/values-secret.yaml ingress-nginx-secret ./ingress-nginx-secret
```
