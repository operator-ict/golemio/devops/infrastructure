# Elastic log management

## create policy logs

PUT _ilm/policy/logs-policy
{
  "policy": {
    "phases": {
      "hot": {
        "min_age": "0ms",
        "actions": {
          "rollover": {
            "max_size": "1gb",
            "max_age": "1d"
          }
        }
      },
      "delete": {
        "min_age": "14d",
        "actions": {
          "delete": {
            "delete_searchable_snapshot": true
          }
        }
      }
    }
  }
}

PUT _ilm/policy/golemio-14_days_retention
{
  "policy": {
    "phases": {
      "hot": {
        "min_age": "0ms",
        "actions": {
          "rollover": {
            "max_size": "1gb",
            "max_age": "14d"
          }
        }
      },
      "delete": {
        "min_age": "14d",
        "actions": {
          "delete": {
            "delete_searchable_snapshot": true
          }
        }
      }
    }
  }
}

PUT _ilm/policy/golemio-long_retention
{
  "policy": {
    "phases": {
      "hot": {
        "min_age": "0ms",
        "actions": {
          "rollover": {
            "max_size": "1gb",
            "max_age": "31d"
          }
        }
      },
      "warm": {
        "min_age": "31d",
        "actions": {}
      },
      "delete": {
        "min_age": "60d",
        "actions": {
          "delete": {
            "delete_searchable_snapshot": true
          }
        }
      }
    }
  }
}

## logs-golemio - data stream

PUT _index_template/logs-golemio-14days
{
  "index_patterns": [
    "logs-golemio-*"
  ],
  "data_stream": {},
  "template": {
    "settings": {
      "index": {
        "lifecycle": {
          "name": "golemio-14_days_retention"
        }
      }
    },
    "mappings": {
      "_size": {
        "enabled": true
      }
    }
  },
  "priority": 900
}

PUT _index_template/logs-golemio-long
{
  "index_patterns": [
    "logs-golemio-long*"
  ],
  "data_stream": {},
  "template": {
    "settings": {
      "index": {
        "lifecycle": {
          "name": "golemio-long_retention"
        }
      }
    },
    "mappings": {
      "_size": {
        "enabled": true
      }
    }
  },
  "priority": 1000
}

## logs-golemio

PUT _index_template/logs-golemio-template
{
  "index_patterns": [
    "golemio-*"
  ],
  "template": {
    "settings": {
      "index": {
        "lifecycle.name": "logs-policy",
        "lifecycle.rollover_alias": "golemio"
      }
    },
    "mappings": {
      "properties": {
        "@timestamp": {
          "type": "date"
        },
        "timestamp": {
          "type": "text"
        },
        "log": {
          "type": "wildcard"
        },
        "message": {
          "type": "wildcard"
        }
      }
    }
  },
  "priority": 500
}

// PUT golemio-000001
// {
//   "aliases": {
//     "golemio": {
//       "is_write_index": true
//     }
//   },
//   "mappings": {
//     "properties": {
//       "@timestamp": {
//         "type": "date"
//       },
//       "timestamp": {
//         "type": "text"
//       },
//       "log": {
//         "type": "wildcard"
//       },
//       "message": {
//         "type": "wildcard"
//       }
//     }
//   }
// }
//
//
// ## logs-parking - data stream
//
// PUT _component_template/logs-parking-mappings
// {
//   "template": {
//     "mappings": {
//       "properties": {
//         "@timestamp": {
//           "type": "date"
//         },
//         "log": {
//           "type": "wildcard"
//         },
//         "message": {
//           "type": "wildcard"
//         }
//       }
//     }
//   }
// }
//
// PUT _component_template/logs-parking-settings
// {
//   "template": {
//     "settings": {
//       "index.lifecycle.name": "logs-policy"
//     }
//   }
// }
//
// PUT _index_template/logs-parking-template
// {
//   "index_patterns": [
//     "parking-logs-*"
//   ],
//   "data_stream": {},
//   "composed_of": [
//     "logs-parking-mappings",
//     "logs-parking-settings"
//   ],
//   "priority": 500
// }
