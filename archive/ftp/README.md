# Instalace
Nejprve nainstalujte storage class:
```bash
kubectl apply -f storageclass.yaml
```
## Rabín
```bash
helm template ftp ftp --values cluster_development/storage_ftp-values.yaml
```
## Golem
```bash
helm upgrade --install -n storage  ftp ftp --values cluster_production/storage_ftp-values.yaml

```

# Reinstalace
Po odinstalování helmu zůstane disk s daty ve stavu `Released`. Aby si ho nová
instalace mohla připojit (a nevytvářela nový), je třeba smazat původní
referenci:
```bash
kubectl patch pv <persistent-volume-name> -p '{"spec":{"claimRef": null}}'
```
