# Covid

# Dev
```bash
helm upgrade --namespace covid  --values covid/values-development-versions.yaml --values cluster_development/covid_covid-values.yaml covid covid
```

# Stage
```bash
helm upgrade --namespace covid-stage --values covid/values-stage-versions.yaml --values cluster_development/covid-stage_covid-values.yaml covid covid
```

# Golem
```bash
helm upgrade --namespace covid --values covid/values-master-versions.yaml --values cluster_production/covid_covid-values.yaml covid covid
```
