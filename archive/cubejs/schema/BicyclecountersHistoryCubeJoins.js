cube(`bikes_history_full`, {
  sql: `SELECT 
          to_timestamp((measured_from / 1000))::date as measured_from,
          locations_id,
          directions_id,
          sum(value) as value 
        from bicycle_counters.bicyclecounters_detections
        group by
          to_timestamp((measured_from / 1000))::date,
          locations_id,
          directions_id`,
  preAggregations: {
    daily_wjoin: {
      measures: [CUBE.detections_sum_cube],
      dimensions: [bikes_locations.name, bikes_locations.lat, bikes_locations.lng, bikes_locations.route, CUBE.year, CUBE.month, CUBE.day, CUBE.weekend],
      timeDimension: CUBE.measured_from,
      granularity: `day`,
      partitionGranularity: `month`,
      refreshKey: {
        every: `1 day`,
        incremental: true,
      }
    },
  },

  joins: {
    bikes_locations: {
      relationship: `belongsTo`,
      sql: `${CUBE.locations_id} = ${bikes_locations.id}`,
    },
    bikes_directions: {
      relationship: `belongsTo`,
      sql: `${CUBE.directions_id} = ${bikes_directions.id}`,
    }
  },
  
  measures: {
    count: {
      type: `count`
    },
    
    detections_sum_cube: {
      sql: `value`,
      type: `sum`
    }
  },
  
  dimensions: {
    id: {
      sql: `${CUBE}.measured_from || '-' || ${CUBE}.locations_id || '-' || ${CUBE}.directions_id`,
      type: `string`,
      primaryKey: true,
    },
    measured_from: {
      sql: `measured_from`,
      type: `time`
    },

    day: {
      sql: `date_part('day', measured_from)`,
      type: `number`
    },
    month: {
      sql: `date_part('month', measured_from)`,
      type: `number`
    },

    year: {
      sql: `date_part('year', measured_from)`,
      type: `number`
    },

    weekend: {
      sql: `case when date_part('isodow', measured_from) in (6, 7) then 'weekend' else 'workday' end`,
      type: `string`
    },

    locations_id: {
      sql: `locations_id`,
      type: `string`
    },

    directions_id: {
      sql: `directions_id`,
      type: `string`
    }
  },
  
  dataSource: `default`
});
