# ARGO WORKFLOWS
## HELM
https://github.com/argoproj/argo-helm/tree/main/charts/argo-workflows
```
kubectl create namespace argo
kubens argo
```
## Installation
```bash
helm repo add argo https://argoproj.github.io/argo-helm
helm repo update
## DEV
helm upgrade --install -n argo argo-workflows argo/argo-workflows --values argo-workflow/values.yaml --values cluster_development/argo-workflow_argo-workflow-values.yaml --version 0.33.3
## PROD
helm upgrade --install -n argo argo-workflows argo/argo-workflows --values argo-workflow/values.yaml --values cluster_production/argo-workflow_argo-workflow-values.yaml --version 0.33.3
```
There is a bug in the helm chart which causes the default configmap *artifact-repositories* not being created, so we have to do it manually:
```
kubectl apply -f ../cluster_development/configmaps/argo-04_configmap.yaml
```
## Quick start
```bash
# create workflow template
argo template create experiments/template-s3tools-zip.yaml

# create workflow
argo submit experiments/wf-s3tools-zip.yaml

# create cron workflow
argo cron lint experiments/cron-s3tools.yaml
argo cron create experiments/cron-s3tools.yaml
```
## Uninstall
```
kubectl delete ingress argo-server-internal-ingress
```
```
kubectl delete configmap artifact-repositories
```
```
helm uninstall argo-workflows
```
Workflows (jobs) are to be deleted separately (manually). See `kubectl api-resources | grep argo`.
