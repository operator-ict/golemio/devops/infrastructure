
# Performance tests

## RUN TESTS

### Setup golemio instance

#### golemio-secrets
```bash
helm upgrade --install \
  --values ../rabin/performance-tests/values-secret.yaml \
  --namespace=performance-tests \
  golemio-secret golemio-secret
```

#### golemio
```bash
helm upgrade --install \
  --values cluster_development/performance-tests_golemio-values.yaml \
  --values golemio/values-vp-development-versions.yaml \
  --namespace=performance-tests \
  golemio ../infrastructure/golemio
```
List of services:
- http://og-perf.rabin-internal.oict.cz
- http://ig-perf.rabin-internal.oict.cz
- http://rabbitmq-perf.rabin-internal.oict.cz

### How sync all fake times?

Run 
```bash
kdel -f performance-tests/migration.yaml 
kdel pod postgresql-0
sleep 5;
kdel pod -l app.kubernetes.io/name=output-gateway-v2-pt &
kdel pod -l app.kubernetes.io/name=integration-engine-vp-pt &
kaf performance-tests/migration.yaml &
```

### Run perf tests
```bash
# --install
helm upgrade --timeout=15m --namespace performance-tests performance-tests performance-tests
```


## Services and prerequisites

## K8S

## K8S nodes
```bash
kubectl label nodes k8s01-worker04 type=performance-tests
kubectl taint nodes k8s01-worker04 performance-tests=true:NoSchedule 
kubectl create namespace performance-tests 
```

## Redis

```bash 
helm repo add bitnami https://charts.bitnami.com/bitnami
helm upgrade --install -n performance-tests --values performance-tests/redis-values.yaml redis bitnami/redis
```


## RabbitMQ
```bash
helm repo add bitnami https://charts.bitnami.com/bitnami

helm install \
  --values performance-tests/rabbitmq-values.yaml \
  --namespace=performance-tests \
  rabbitmq bitnami/rabbitmq
```

## PostgreSQL

[Github repo](https://github.com/bitnami/charts/tree/master/bitnami/postgresql/)

```bash
helm repo add bitnami https://charts.bitnami.com/bitnami

helm upgrade --install \
  --values performance-tests/postgresql-values.yaml \
  --namespace=performance-tests \
  postgresql bitnami/postgresql
  
  
TBD nastavit nad DB: CREATE EXTENSION IF NOT EXISTS EXTENSION postgis;


CREATE EXTENSION IF NOT EXISTS postgres_fdw;

CREATE SERVER IF NOT EXISTS vehicle_positions_rabin
    FOREIGN DATA WRAPPER postgres_fdw
    OPTIONS (host '10.228.1.31', dbname 'vehicle-positions', port '5432');

CREATE USER MAPPING IF NOT EXISTS FOR postgres
    SERVER vehicle_positions_rabin
    OPTIONS (user 'read_only', password 'pass');
```
