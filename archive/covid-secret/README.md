helm upgrade --install --namespace covid --values ../myfolder/values.yml -secret .


# rabin
helm upgrade --install --namespace covid --values ../rabin/covid/values-secret.yaml covid-secret covid-secret

# stage
helm upgrade --install --namespace covid-stage --values ../rabin/covid-stage/values-secret.yaml covid-stage-secret covid-secret

# golem
helm upgrade --install --namespace covid --values ../golem/covid/values-secret.yaml covid-secret covid-secret 

