# Rabin
```bash
helm upgrade -n golemio --install --values ./postgres-alerts/values-master-versions.yaml --values cluster_development/golemio_postgres-alerts-values.yaml postgres-alerts ./postgres-alerts
```

# Golem
```bash
helm upgrade -n golemio --install --values ./postgres-alerts/values-master-versions.yaml --values cluster_production/golemio_postgres-alerts-values.yaml postgres-alerts ./postgres-alerts
```
