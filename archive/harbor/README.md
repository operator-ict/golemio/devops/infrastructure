# Harbor proxy cache pro docker images
```bash
helm upgrade --install -n harbor harbor bitnami/harbor --values harbor/values.yaml
```
Credentials pro připojení k Harbor jsou řešeny stejně jako pro GitLab, každý
Helm chart má v sobě obdobně `harbor-registry` secret a v
cluster_(development|production) je pak override v sekci `global`:
```
global:
  imageRegistry: <Harbor FQDN>/gitlab_proxy_cache/operator-ict/...
  imagePullSecrets: harbor-registry
```
### Rabín
https://harbor.rabin.golemio.cz
