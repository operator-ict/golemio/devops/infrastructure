## Fluent-bit

https://github.com/fluent/helm-charts/tree/main/charts/fluent-bit

Installation:
```
helm repo add fluent https://fluent.github.io/helm-charts
helm upgrade --install -n monitoring fluent-bit fluent/fluent-bit --values monitoring/fluentbit/values.yaml --version=^0.29
```