# ARGO CD

## Installation
```bash
## DEV
helm upgrade --install -n argocd --values https://gitlab.com/operator-ict/devops/infrastructure/-/raw/main/argocd/common.yaml --values cluster_development/argocd_argocd-values.yaml argocd argo/argo-cd --version 7.4.1

## PROD
helm upgrade --install -n argocd --values https://gitlab.com/operator-ict/devops/infrastructure/-/raw/main/argocd/common.yaml --values cluster_production/argocd_argocd-values.yaml argocd argo/argo-cd --version 7.4.1
```
