# Golemio - database

## Adminer

```shell
k apply -f adminer.yml --namespace database
```

## Redis
https://github.com/bitnami/charts/tree/main/bitnami/redis

```bash 
helm repo add bitnami https://charts.bitnami.com/bitnami

# create storage class
kaf database/storageClass.yaml

# Rabin
helm upgrade --version=19.1.5  --install -n database --values cluster_development/database_redis-values.yaml redis bitnami/redis

# Golem
helm upgrade --version=19.1.5 --install -n database --values cluster_production/database_redis-values.yaml redis bitnami/redis

# parking Rabin
helm upgrade --version=19.1.5  --install -n parking-dev --values cluster_development/parking-dev_redis-values.yaml redis-parking bitnami/redis

# parking Golem
helm upgrade --version=19.1.5 --install -n parking     --values cluster_production/parking_redis-values.yaml redis bitnami/redis
```

# PSQL
## Major upgrade
1) vypnout služby -- optional
2) truncate tabulek positions, trips a redis -- optional
3) možné započít migraci
4) zavolat VACUUM FULL; ANALYSE VERBOSE; -- pro jistotu VACUUM, ANALYSE je povinne
5) zkontrolovat a vyprázdnit zprávy v rabbitovi -- optional
6) spustit znovu všechny služby

## PGBouncer
```bash
psql "host=psql-vp2-golem.postgres.database.azure.com port=6432 dbname=pgbouncer user=spravce password=FILL_PASSWORD sslmode=require"
```
