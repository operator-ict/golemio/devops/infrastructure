
# Rabin
```bash
helm upgrade --install --values ../rabin/data-proxy/values-secret.yaml -n golemio data-proxy-secret ./data-proxy-secret
```

# Golem
```bash
helm upgrade --values ../golem/data-proxy/values-secret.yaml -n golemio data-proxy-secret ./data-proxy-secret
```
