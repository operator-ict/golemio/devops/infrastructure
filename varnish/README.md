# Varnish

## Rabin

### Rabin - MAPK2O
```bash
helm upgrade --install -n mapk2o            --values cluster_development/mapk2o_varnish.yaml mapk2o-varnish ./varnish
helm upgrade --install -n golemio           --values cluster_development/golemio_varnish.yaml golemio-varnish ./varnish
helm upgrade --install -n vehicle-positions --values cluster_development/vehicle-positions_varnish.yaml vehicle-positions-varnish ./varnish
```

## Golem

```bash
helm upgrade --install -n mapk2o            --values cluster_production/mapk2o_varnish.yaml varnish ./varnish
helm upgrade --install -n golemio           --values cluster_production/golemio_varnish.yaml varnish ./varnish
helm upgrade --install -n vehicle-positions --values cluster_production/vehicle-positions_varnish.yaml varnish ./varnish
```

## Další materiály
- https://hub.docker.com/_/varnish
- https://github.com/softonic/varnish-chart/tree/master
- https://www.varnish-software.com/developers/tutorials/varnish-builtin-vcl/

## Prometheus
- vše co jsem našel má poslední update z roku 2021 nebo je to Vanish enterprise

## Clustering 
- https://dealancer.medium.com/creating-a-scalable-and-resilient-varnish-cluster-using-kubernetes-853f03ec9731
- https://info.varnish-software.com/blog/creating-self-routing-varnish-cluster

## VCL steps
- https://varnish-cache.org/docs/trunk/reference/vcl-step.html#id7

## Debug
https://janosmiko.com/blog/2021-10-01-how-to-debug-varnish/
```bash
varnishlog -q 'ReqURL eq "/sortedwastestations"'
```