# Curl 

## Měření odezvy
dle https://stackoverflow.com/questions/18215389/how-do-i-measure-request-and-response-times-at-once-using-curl/22625150#22625150

```bash
curl -w "@curl-format.txt" -o /dev/null -s "http://golemio.cz/"
```

## SSL certifikát
```bash
echo -n | openssl s_client -connect golemio.cz:443 -servername golemio.cz | openssl x509
```