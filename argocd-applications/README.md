# Rabin
```bash
helm upgrade --install -n argocd --values ./cluster_development/argocd_argocd-applications-values.yaml argocd-applications ./argocd-applications
```

# Golem
```bash
helm upgrade --install -n argocd --values ./cluster_production/argocd_argocd-applications-values.yaml argocd-applications ./argocd-applications
```
