{{/* vim: set filetype=mustache: */}}

{{- define "syncPolicy" }}
{{- $values := index . 1 -}}
{{- $globalSyncOptions := index . 2 -}}
syncPolicy:
  {{- if and $values (default false $values.automated ) }}
  automated:
    prune: {{ $values.prune | default false }}
    selfHeal: {{ $values.selfHeal | default false }}
  {{- end }}
  syncOptions:
  {{- range $key, $value :=  merge (default dict $values.options) (deepCopy $globalSyncOptions)  }}
    - {{ $key }}={{ $value }}
  {{- end }}
{{- end -}}
